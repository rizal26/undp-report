$(document).ready(function () {
    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                'HRU',
                'ICT',
                'FRMU',
                'PROC',
                'ADM',
                'PMEU',
                'CSA',
                'COMM',
                'Partership',
                'REDD+',
                'QARE',
            ],
            datasets: [
                {
                    label: 'Completed',
                    backgroundColor: '#28a74559',
                    borderColor: 'green',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'In Progress',
                    backgroundColor: '#ffc10759',
                    borderColor: 'yellow',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'Cancelled',
                    backgroundColor: '#fd7e1459',
                    borderColor: 'orange',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'Rejected',
                    backgroundColor: '#dc354559',
                    borderColor: 'red',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'Returned',
                    backgroundColor: '#6f42c159',
                    borderColor: 'purple',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'New Request',
                    backgroundColor: '#6c757d59',
                    borderColor: 'gray',
                    borderWidth: 1,
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100))
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 100,
                        // reverse: false,
                        stepSize: 20
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage of Requests'
                    }
                }]
            },
            legend: {
                position: 'bottom'
            }
        }
    });
});
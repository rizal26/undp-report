$(document).ready(function () {
    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                'HRU',
                'ICT',
                'FRMU',
                'PROC',
                'ADM'
            ],
            datasets: [
                {
                    label: 'On Time / Faster',
                    backgroundColor: '#28a74559',
                    borderColor: 'green',
                    borderWidth: 1,
                    data: Array.from({ length: 5 }, () => Math.floor(Math.random() * 100))
                },
                {
                    label: 'Delay',
                    backgroundColor: '#fd7e1459',
                    borderColor: 'orange',
                    borderWidth: 1,
                    data: Array.from({ length: 5 }, () => Math.floor(Math.random() * 100))
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 100,
                        // reverse: false,
                        stepSize: 20
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage of Requests'
                    }
                }]
            },
            legend: {
                position: 'bottom'
            }
        }
    });
});
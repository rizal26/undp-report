$(document).ready(function () {
    
    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 11; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                'HRU',
                'ICT',
                'FRMU',
                'PROC',
                'ADM',
                'PMEU',
                'CSA',
                'COMM',
                'Partership',
                'REDD+',
                'QARE',
            ],
            datasets: [
                {
                    label: '1',
                    data: Array.from({ length: 11 }, () => Math.floor(Math.random() * 100)),
                    backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                        ],
                    borderWidth: 1,
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 100,
                        // reverse: false,
                        stepSize: 20
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Percentage of Requests'
                    }
                }]
            },
            legend: {
                position: 'bottom'
            }
        }
    });
});
$(document).ready(function() {
    // var loading = '<div class="text-center" style="font-size: 56px;"><i class="fa fa-spin fa-refresh"></i></div>';
    $('#detail').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/detail.html');
    });

    $('#timeliness-report').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/timeliness-report.html');
    });

    $('#timeliness-detail').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/timeliness-detail.html');
    });

    $('#coa').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/coa.html');
    });

    $('#registration').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/registration.html');
    });

    $('#workload').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/workload.html');
    });

    $('#performance').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/performance.html');
    });

    $('#progress').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/progress.html');
    });

    $('#cost').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/cost.html');
    });

    $('#service').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/service.html');
    });

    $('#tracking').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/tracking.html');
    });

    $('#search').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/search.html');
    });

    $('#invoice').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/invoice.html');
    });

    $('#dsa').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/dsa.html');
    });

    $('#complete').click(function(e) {
        e.preventDefault();
        // $('.content').html(loading);
        $('.content').load('content/complete.html');
    });

    // $('a').click(function (e) { 
    //     e.preventDefault();
    //     $(this).css('color', 'black');
    //     $(this).css('font-weight', 'bold');
    // });
});